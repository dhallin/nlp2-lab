from model.build_dictionary import build_vecdicts

__author__ = 'bas'
from codecs import open
from collections import defaultdict
import random

import gensim
from nltk.tokenize import wordpunct_tokenize
from sklearn.cluster import *
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

import time

from model.europarl_corpus import Corpus
import os
from translation_model import TranslationModel, translation_model_dump_available
from utils.vec2pca import Vec2PCA


def run_experiment():
    l_source = 'de'
    l_target = 'en'

    corpus_limit = 10000
    c_source = Corpus(language=l_source, n_limit=corpus_limit)
    c_target = Corpus(language=l_target, n_limit=corpus_limit)

    m_source = c_source.word2vec()
    m_target = c_target.word2vec()

    # Build dictionary indexes
    dict_indexes_source, dict_indexes_target, dictionary = build_vecdicts(m_source, m_target, l_source, l_target)

    print m_source.most_similar('gemeinsamen')
    print m_target.most_similar('common')

    # TODO: Use phrases: https://radimrehurek.com/gensim/models/phrases.html#module-gensim.models.phrases

    # Find word alignments

    print m_source.syn0[dict_indexes_source]
    # ...

    # Train linear regression

    translation_model = TranslationModel(l_source, l_target,
                                         m_source, m_target,
                                         dict_indexes_source, dict_indexes_target,
                                         dictionary,
                                         cache=False, dump_load=True, dump_save=True)
    # translation_model.train_model()

    word = translation_model.dictionary.items()[0][0]

    candidates, scores = translation_model.candidates(word)
    print word
    print candidates, scores

    vec2pca = Vec2PCA(translation_model.source_model.syn0[:100],
                      translation_model.dictionary.keys())
    vec2pca.plot()

    # Get different candidate rankings. cosine-sim, 5 best, etc.


def pre_comp(l_source, l_target, corpus_limit):
    start_time = time.time()
    c_source = Corpus(language=l_source, n_limit=corpus_limit)
    c_target = Corpus(language=l_target, n_limit=corpus_limit)
    m_source = c_source.word2vec()
    m_target = c_target.word2vec()
    # Following line is never cached on its own
    dict_indexes_source, dict_indexes_target, dictionary = build_vecdicts(m_source, m_target, l_source, l_target)
    _ = TranslationModel(l_source, l_target,
                         m_source, m_target,
                         dict_indexes_source, dict_indexes_target,
                         dictionary,
                         dump_load=False, dump_save=True)
    stop_time = time.time()
    print "Done in %d" % (stop_time - start_time)

if __name__ == "__main__":

    pre_comp('de', 'en', None)
#    run_experiment()