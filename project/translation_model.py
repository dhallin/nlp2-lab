from gensim import matutils
import sys

__author__ = 'Daniel'

import numpy as np
from sklearn.linear_model import LinearRegression
from gensim.models import word2vec
from model import build_dictionary
import os
import cPickle as pickle


class TranslationModel(object):
    CACHE_DIRECTORY = "/home/veeling/nlp2-lab/project/model/cache"  # Cache weights
    DUMP_DIRECTORY = "/home/veeling/nlp2-lab/project/model/dumb"  # Cache complete translation model

    def __init__(self, source_lang, target_lang, source_model=None, target_model=None,
                 source_idxs=None, target_idxs=None, dictionary=None,
                 cache=True,  dump_load=False, dump_save=False):

        self.weights = None
        self.source_lang = source_lang
        self.target_lang = target_lang

        self.dump_load = dump_load
        self.dump_save = dump_save
        if cache:
            self.dump_load = True
            self.dump_save = True

        self.source_model = None
        self.target_model = None
        self.source_idxs = None
        self.target_idxs = None
        self.dictionary = None

        if self.dump_load:
            try:
                self._dump_load()
                self.dump_save = False
                # print "Succesfully loaded model"
            except Exception:
                sys.stderr.write("Couldn't load translation model, attempting standard initialisation")
                self.source_model = source_model
                self.target_model = target_model
                self.source_idxs = source_idxs
                self.target_idxs = target_idxs
                self.dictionary = dictionary
                self.cache = cache

                self.train_model()
        else:
            self.source_model = source_model
            self.target_model = target_model
            self.source_idxs = source_idxs
            self.target_idxs = target_idxs
            self.dictionary = dictionary
            self.cache = cache

            self.train_model()

    def load_cache(self):
        if not os.path.isdir(TranslationModel.CACHE_DIRECTORY):
            os.makedirs(TranslationModel.CACHE_DIRECTORY)

        cache_file_path = "%s/transmodel_%s_%s.cpickle" % (
            TranslationModel.CACHE_DIRECTORY, self.source_lang, self.target_lang)
        path = os.path.abspath(os.path.join(os.curdir, cache_file_path))

        if os.path.isfile(path):
            self.weights = pickle.load(open(path, 'rb'))
        else:
            raise Exception("No cache found")

    def train_model(self, source_model=None, target_model=None, source_idxs=None, target_idxs=None,):

        self._complete_model(source_model, target_model, source_idxs, target_idxs)
        if None in [self.source_model, self.target_model, self.source_idxs, self.target_idxs]:
            raise Exception("Incomplete translation model")

        source_vectors = self.source_model.syn0[self.source_idxs]
        target_vectors = self.target_model.syn0[self.target_idxs]

        self.train_model_with_vectors(source_vectors, target_vectors)

        if self.dump_save:
            self._dump_save()

    def train_model_with_vectors(self, source_vectors, target_vectors):

        # assertions
        assert len(np.shape(source_vectors)) == 2, "Should be matrix"
        assert len(np.shape(target_vectors)) == 2, "Should be matrix"

        assert np.shape(source_vectors) == np.shape(target_vectors), "Matrices should be same size"

        # start training model
        print "\nStarted training model"

        d = np.shape(source_vectors)[1]  # number of dimensions
        self.weights = np.zeros((d, d))  # init weights

        linear_regression = LinearRegression(fit_intercept=True,    # init linear regression model
                                             normalize=False,
                                             copy_X=True,
                                             n_jobs=1)

        for i in range(d):
            print "\tTraining regression model %d/%d" % (i + 1, d)

            t = target_vectors[:, i]                         # the ith column of target_vectors
            x = source_vectors                               # the source_vectors

            linear_regression.fit(x, t)                     # fit the model

            self.weights[i, :] = linear_regression.coef_     # put the weight coefficients in the weight matrix

        print "\nFinished training model"
        if self.cache:
            cache_file_path = "%s/transmodel_%s_%s.cpickle" % (
                TranslationModel.CACHE_DIRECTORY, self.source_lang, self.target_lang)
            pickle.dump(self.weights, open(cache_file_path,'wb'))

        if self.dump_save:
            self._dump_save()

    def candidates(self, source_word, num_candidates=3):

        assert self.weights is not None, "Translation model should first be trained"

        source_word_vector = self.source_model[source_word]                              # compute vector
        target_word_vector = self.weights.dot(source_word_vector)                   # translate vector

        target_vectors = self.target_model.syn0[self.target_idxs]

        similarities = target_vectors.dot(target_word_vector)                       # compute similarities

        relative_idxs = similarities.argsort()[-num_candidates:][::-1]              # find best relative indexes

        candidate_idxs = [self.target_idxs[idx] for idx in relative_idxs]
        candidate_words = [self.target_model.index2word[idx] for idx in candidate_idxs]  # translate to words
        similarity_scores = similarities[relative_idxs]                             # similarity scores

        return candidate_words, similarity_scores

    def get_score(self, source_embed, target_embed):
        assert self.weights is not None, "Translation model should first be trained"
        est_target = self.weights.dot(source_embed)
        est_target_norm = matutils.unitvec(est_target)
        target_embed_norm = matutils.unitvec(target_embed)
        return np.dot(est_target_norm, target_embed_norm), est_target

    def _complete_model(self, source_model, target_model, source_idxs, target_idxs):
        if source_model is not None:
            self.source_model = source_model
        if target_model is not None:
            self.target_model = target_model
        if source_idxs is not None:
            self.source_idxs = source_idxs
        if target_idxs is not None:
            self.target_idxs = target_idxs

    def _dump_load(self):
        directory, contents = _dump_directory(self.source_lang, self.target_lang)
        dump_path = "%s/%s" % (directory, contents[-1])
        if len(contents) == 0:
            raise Exception("No dump found")
        if not os.path.isfile(dump_path):
            raise Exception("No dump found")

        parameters = pickle.load(open(dump_path, 'rb'))

        self.weights = parameters["weights"]
        self.source_lang = parameters["source_lang"]
        self.target_lang = parameters["target_lang"]
        self.source_model = parameters["source_model"]
        self.target_model = parameters["target_model"]
        self.source_idxs = parameters["source_idxs"]
        self.target_idxs = parameters["target_idxs"]
        self.cache = parameters["cache"]
        self.dictionary = parameters["dictionary"]

    def _dump_save(self):
        self.save = False
        directory, contents = _dump_directory(self.source_lang, self.target_lang)

        dump_file_path = "%s/transmodel_%s_%s_%d.dump" % (directory,
                                                          self.source_lang,
                                                          self.target_lang,
                                                          len(contents))
        parameters = dict()
        parameters["weights"] = self.weights
        parameters["source_lang"] = self.source_lang
        parameters["target_lang"] = self.target_lang
        parameters["source_model"] = self.source_model
        parameters["target_model"] = self.target_model
        parameters["source_idxs"] = self.source_idxs
        parameters["target_idxs"] = self.target_idxs
        parameters["cache"] = self.cache
        parameters["dictionary"] = self.dictionary

        pickle.dump(parameters, open(dump_file_path, 'wb'))


def _dump_directory(source_lang, target_lang):

    dump_path = os.path.abspath(os.path.join(os.curdir, TranslationModel.DUMP_DIRECTORY))
    if not os.path.isdir(dump_path):
        os.makedirs(dump_path)

    sub_directory = "%s_%s" % (source_lang, target_lang)
    full_directory = os.path.abspath(os.path.join(dump_path, sub_directory))

    if not os.path.isdir(full_directory):
        os.makedirs(full_directory)

    contents = [filename for filename in os.listdir(full_directory)
                if os.path.isfile(os.path.join(full_directory, filename))]

    return full_directory, contents


def translation_model_dump_available(source_language, target_language):
    directory, contents = _dump_directory(source_language, target_language)
    return len(contents) > 0


def test_model():
    test_with_mockup()


def test_with_mockup():

    print "\nTesting model"

    mu = np.array([5.0, 0.0, 10.0])       # test data
    r = np.array([
        [  3.40, -2.75, -2.00],
        [ -2.75,  5.50,  1.50],
        [ -2.00,  1.50,  1.25]
    ])

    num_samples = 1000

    w = np.random.randn(3, 3)             # test generating model

    source = np.random.multivariate_normal(mu, r, size=num_samples)         # generate the random samples.

    target = np.array([source[i, :].dot(w) for i in range(num_samples)])    # generate linear correspondence

    model = TranslationModel('a', 'b', cache=False, dump_save=True, dump_load=True)                                              # init translation model
    model.train_model_with_vectors(source, target)                          # train model

    # print results

    for i in range(5):
        print "\ny = W * x"
        print target[i, :]
        print model.weights.dot(source[i, :])

    print "\nAverage Error"
    print np.mean(target - source.dot(model.weights.T))


if __name__ == "__main__":

    # Test model
    test_model()
