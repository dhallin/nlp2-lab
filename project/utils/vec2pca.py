__author__ = 'Daniel'

import numpy as np
import matplotlib.pyplot as plt

from sklearn.decomposition import PCA


class Vec2PCA:

    def __init__(self, vectors=None, wordlist=None, n_components=2):
        self.pca = PCA(n_components=n_components)
        self.components = []
        self.word2component = dict()

        self.vectors2components(vectors, wordlist)

    def vectors2components(self, vectors, wordlist=None):
        if vectors is not None:
            self.components = self.pca.fit_transform(vectors)
            if wordlist is not None:
                assert len(wordlist) == np.shape(self.components)[0], "wordlist needs to be same length as vectors"
                self.word2component = dict(zip(wordlist, self.components))

        return self.components

    def plot(self, list_items=None):

        fig, ax = plt.subplots()
        ax.scatter(self.components[:, 0],
                   self.components[:, 1])

        for word, component in self.word2component.iteritems():
            ax.annotate(word, component)

        plt.show()


def test():
    vectors = np.random.random((100, 10))
    v2pca = Vec2PCA(vectors)

    assert np.shape(v2pca.components) == (100, 2)
    print vectors[:2]
    print v2pca.components[:2]


def main():
    pass


if __name__ == "__main__":
    test()
    main()
