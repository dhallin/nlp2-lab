import os
import stat
import time
from translation_model import TranslationModel


def watch2(file_name, file_length=None, sleep_time=0.000001):
    # prepare to see if file grows, bail out if next file appears and nothing else to send
    file_length = file_length or (os.stat(file_name)[stat.ST_SIZE] if os.path.exists(file_name) else 0)
    new_length = file_length
    rest = ""
    while True:
        if new_length > file_length:
            with open(file_name) as f:
                f.seek(file_length)
                new_buffer = f.read(new_length - file_length)
            last_break = new_buffer.rfind("\n")
            if last_break == -1:
                new_lines = []
                rest = rest + new_buffer
            else:
                new_lines = (rest + new_buffer[:last_break]).split("\n")
                rest = new_buffer[last_break + 1:]
            file_length = new_length
            for l in new_lines:
                yield l
        else:
            time.sleep(sleep_time)
        new_length = os.stat(file_name)[stat.ST_SIZE] if os.path.exists(file_name) else 0

    if rest:
        yield rest


def callback(t):
    print t
    source_phrase, target_phrase = tuple(t.split(SEP))
    try:
        source_embed = m_source[source_phrase]
        target_embed = m_target[target_phrase]

        score, _ = translation_model.get_score(source_embed, target_embed)
    except:
        score = 0.0
    with open(RESULTS_FILE,'a') as f:
        f.writelines([str(score)+"\n"])


SEP = '!@#%!@%)*#$'
RESULTS_FILE = '/home/veeling/results.out'
REQUEST_FILE = '/home/veeling/requests.out'
if __name__ == '__main__':
    translation_model = TranslationModel('de', 'en', dump_load=True)
    # translation_model.load_cache()
    m_source = translation_model.source_model
    m_target = translation_model.target_model
    file_length = os.stat(REQUEST_FILE)[stat.ST_SIZE] if os.path.exists(REQUEST_FILE) else 0
    for t in watch2(REQUEST_FILE,file_length):
        callback(t)