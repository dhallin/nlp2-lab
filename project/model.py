__author__ = 'Daniel'

import numpy as np
from sklearn.linear_model import LinearRegression
from gensim.models import word2vec
from model import build_dictionary

class Model(object):
    def __init__(self):
        self.weights = None

    def train_model(self, source_model, target_model, source_idxs, target_idxs):
        source_vectors = source_model[source_idxs]
        target_vectors = target_model[target_idxs]

        self.train_model_with_vectors(source_vectors, target_vectors)

    def train_model_with_vectors(self, source_vectors, target_vectors):

        # assertions

        assert len(np.shape(source_vectors)) == 2, "Should be matrix"
        assert len(np.shape(target_vectors)) == 2, "Should be matrix"

        assert np.shape(source_vectors) == np.shape(target_vectors), "Matrices should be same size"

        # start training model

        print "\nStarted training model"

        d = np.shape(source_vectors)[1]                     # number of dimensions
        self.weights = np.zeros((d, d))                     # init weights

        linear_regression = LinearRegression(fit_intercept=True,    # init linear regression model
                                             normalize=False,
                                             copy_X=True,
                                             n_jobs=1)

        for i in range(d):
            print "\tTraining regression model %d/%d" % (i + 1, d)

            t = target_vectors[:, i]                         # the ith column of target_vectors
            x = source_vectors                               # the source_vectors

            linear_regression = linear_regression.fit(x, t)  # fit the model

            self.weights[i, :] = linear_regression.coef_     # put the weight coefficients in the weight matrix

        print "\nFinished training model"

    def candidates(self, source_word, source_model, target_model, source_vectors, target_vectors, num_candidates=5):

        assert self.weights is not None, "Translation model should first be trained"

        assert np.shape(source_vectors) == np.shape(target_vectors), "Source and target vectors should be same shape"

        source_word_vector = source_model[source_word]                              # compute vector
        target_word_vector = self.weights.dot(source_word_vector)                   # translate vector

        similarities = target_vectors.dot(target_word_vector)                       # compute similarities

        candidate_idxs = similarities.argsort()[-num_candidates:][::-1]             # find best candidates vectors

        candidate_words = [target_model.idx2word(idx) for idx in candidate_idxs]    # translate to words
        similarity_scores = similarities[candidate_idxs]                            # similarity scores

        return candidate_words, similarity_scores


def test_model():
    test_with_mockup()


def test_with_mockup():

    print "\nTesting model"

    mu = np.array([5.0, 0.0, 10.0])       # test data
    r = np.array([
        [  3.40, -2.75, -2.00],
        [ -2.75,  5.50,  1.50],
        [ -2.00,  1.50,  1.25]
    ])

    num_samples = 1000

    w = np.random.randn(3, 3)             # test generating model

    source = np.random.multivariate_normal(mu, r, size=num_samples)         # generate the random samples.

    target = np.array([source[i, :].dot(w) for i in range(num_samples)])    # generate linear correspondence

    model = Model()                                                         # init translation model
    model.train_model_with_vectors(source, target)                                       # train model

    # print results

    print "\ny = W * x"
    print target[0, :]
    print model.weights.dot(source[0, :])

    print "\nAverage Error"
    print np.mean(target - model.weights.dot(source[0, :]))


if __name__ == "__main__":

    # Test model
    test_model()
