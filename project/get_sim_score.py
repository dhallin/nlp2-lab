import os
import stat
import sys

from wait_for import watch2, SEP, RESULTS_FILE, REQUEST_FILE

__author__ = 'bas'


def callback(t):
    print t
    sys.exit(0)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Give a score for two phrases')
    parser.add_argument('source_phrase', help='', type=str)
    parser.add_argument('target_phrase', help='', type=str)

    args = parser.parse_args()
    source_phrase = args.source_phrase
    target_phrase = args.target_phrase

    # Write to file
    file_length = os.stat(RESULTS_FILE)[stat.ST_SIZE] if os.path.exists(RESULTS_FILE) else 0
    with open(REQUEST_FILE, 'a') as f:
        f.writelines(["%s%s%s\n" % (source_phrase, SEP, target_phrase)])
    for t in watch2(RESULTS_FILE, file_length):
        callback(t)

        # print(source_embed, target_embed)
