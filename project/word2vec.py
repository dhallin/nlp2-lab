__author__ = 'bas'
from codecs import open
from collections import defaultdict
import random

import gensim
from nltk.tokenize import wordpunct_tokenize
from sklearn.cluster import *
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer


def create_affinity_matrix(model, vocab_length):
    affinity_matrix = np.zeros((vocab_length, vocab_length))
    model.init_sims()
    for row in range(vocab_length):
        affinity_matrix[row] = (np.arccos(np.dot(model.syn0norm, gensim.matutils.unitvec(model.syn0[row]))) / np.pi)

    inds = np.where(np.isnan(affinity_matrix))
    affinity_matrix[inds] = 1.
    return affinity_matrix


if __name__ == "__main__":
# 0) gather tokens
    check_words = ['jan', 'april', 'august', 'today', 'tonight', 'tomorrow', 'every', 'starting', 'monday', 'tue', 'at',
                   'dec', 'winter']
    # check_words = ['jan','dec','vandaag']
    lang = "en" + ","

    date_strings = [line.rstrip('\n').lstrip(lang) for line in open('data/all_invalid_dates.csv', 'r', encoding='utf-8') if
                    line.startswith(lang)]

    date_strings = date_strings
    token_strings = []
    token_count = defaultdict(int)
    for date_string in date_strings:
        tokens = wordpunct_tokenize(date_string.lower(), )
        for token in tokens:
            token_count[token] += 1
        token_strings.append(tokens)
    # bigram_transformer = gensim.models.Phrases(token_strings)

# 1) Learn semantic wordspace with word2vec
    print "Starting word2vec"
    model = gensim.models.Word2Vec(token_strings, min_count=10, workers=8)
    model.save('tmp/mymodel')
    print "Done!"
    vocab_length = model.syn0.shape[0]

 # 2) Cluster words based on word2vec similarity
    model.init_sims()
    word_vectors = model.syn0norm

    # Initalize a k-means object and use it to extract centroids
    # clusterer = AgglomerativeClustering(n_clusters=num_clusters,linkage='complete')
    # clusterer = SpectralClustering(n_clusters=num_clusters,linkage='complete')
    preferences = [token_count[word] for word in model.index2word]
    preferences = np.array(preferences) / float(np.max(preferences)) - 1.7837
    clusterer = AffinityPropagation(verbose=True, damping=.7, max_iter=1000)  #,preference=preferences)

    idx = clusterer.fit_predict(word_vectors)

    word_centroid_map = dict(zip(model.index2word, idx))

    def print_cluster(cluster, word_centroid_map):
        # Print the cluster number
        # Find all of the words for that cluster number, and print them out
        words = []
        for i in xrange(0, len(word_centroid_map.values())):
            if ( word_centroid_map.values()[i] == cluster ):
                words.append(word_centroid_map.keys()[i])
        print words

    for cluster in range(len(clusterer.cluster_centers_indices_)):
        print '\n cluster:', cluster
        print_cluster(cluster, word_centroid_map)

    for word in check_words:
        print "\nCheckword:", word, ", cluster:", word_centroid_map[word]
        print_cluster(word_centroid_map[word], word_centroid_map)

# 3) Cluster datestrings on token centroids
    def centroid_tokenizer(text):
        tokens = wordpunct_tokenize(text)
        result = []
        for token in tokens:
            centroid = word_centroid_map.get(token, -1)
            result.append(str(centroid))
        return result

    vectorizer = TfidfVectorizer(tokenizer=centroid_tokenizer,  # stop_words=stopwords.words('english'),
                                 max_df=.9,  # Maximum percentage of word occurrence for vocabulary
                                 min_df=0.005,  # Minimum percentage of word occurrence for vocabulary
                                 preprocessor=None, ngram_range=(2, 5))

    tfidf_model = vectorizer.fit_transform(date_strings)
    # km_model = KMeans(n_clusters=100, verbose=1, tol=1e-5, max_iter=50,n_init=5 )
    km_model = MiniBatchKMeans(n_clusters=100, verbose=1, tol=0., max_iter=5000, n_init=10, batch_size=20000)
    # km_model = AffinityPropagation(verbose=True,damping=.5,max_iter=1)
    km_model.fit(tfidf_model)

    clustering = defaultdict(list)

    for idx, label in enumerate(km_model.labels_):
        clustering[label].append(idx)

    for cluster, docs in clustering.items():
        print cluster, "size:", len(docs)
        selection = random.sample(docs, 30) if len(docs) > 30 else docs
        for doc_index in selection:
            print "\t\t", date_strings[doc_index], '\t', centroid_tokenizer(date_strings[doc_index].lower())
    #
    # for cluster, docs in clustering.items():
    #     print cluster, "size:", len(docs)
    #     selection = random.sample(docs, 30) if len(docs) > 30 else docs
    #     for doc_index in selection:
    #         print "\t\t", centroid_tokenizer(date_strings[doc_index])
    print len(date_strings)

    for cluster, docs in clustering.items():
        print cluster, "size:", len(docs)
        cnt = defaultdict(int)
        for doc in docs:
            for ind in tfidf_model[doc].indices:
                cnt[ind] += 1

        break