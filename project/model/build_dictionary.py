__author__ = 'bas'
import gensim
import json

from collections import Counter
from dictcc import *

import urllib2, urllib
import numpy as np


def build_dictionary(counted):
    dictionary = dict()

    for word, _ in counted.most_common():

        response = urllib2.urlopen(
            "https://glosbe.com/gapi/translate?from=nld&dest=eng&format=json&phrase=%s&pretty=true&tm=false" % word)
        data = json.load(response)
        try:
            dictionary[word] = data['tuc'][0]['phrase']['text']
        except:
            print "can't find %s" % word
    return dictionary


def build_dictionary_cc(counted, source_lang, target_lang):

    dictionary = dict()

    cc_dict = Dict  # dictcc Dictionary

    # build_dictionary()
    for word, _ in counted.most_common(n=100):
        # if ' ' in word:
        #     word = "'%s'" % word
        try:
            translation_result = cc_dict.translate(_string2utf8(word), source_lang, target_lang)
            if len(translation_result.translation_tuples):
                _, translation = translation_result.translation_tuples[0]
                dictionary[word] = translation
        except UnavailableLanguageError:
            print UnavailableLanguageError
            return dictionary
        except urllib2.HTTPError, error:
            contents = error.read()
            print contents

    return dictionary


def build_vecdicts(m_source, m_target, source_language_code, target_language_code):
    counted = Counter()
    for key, val in m_source.vocab.items():
        counted[key] = val.count
    # dictionary = build_dictionary(counted)

    dictionary = build_dictionary_cc(counted, source_language_code, target_language_code)

    # Create matrices

    dict_indexes_source = []
    dict_indexes_target = []

    for i, words in enumerate(dictionary.items()):
        word_source, word_target = words
        # TODO: do we want the normalized vectors here or the normal ones?
        # TODO: keyerror
        if word_target in m_target.vocab and word_source in m_source.vocab:
            dict_indexes_target.append(m_target.vocab[word_target].index)
            dict_indexes_source.append(m_source.vocab[word_source].index)
        else:
            if word_target not in m_target.vocab:
                print "Not in m_target.vocab: %s" % word_target
            else:
                print "Not in m_source.vocab: %s" % word_source

    return dict_indexes_source, dict_indexes_target, dictionary


# helper method
def _string2utf8(string):
    return string.encode("utf8", "ignore")


def main():
    source_lang = "de"
    target_lang = "en"

    m_source = gensim.models.Word2Vec.load('tmp/%s.model' % source_lang)
    m_target = gensim.models.Word2Vec.load('tmp/%s.model' % target_lang)
    m_source.init_sims()
    m_target.init_sims()
    print build_vecdicts(m_source, m_target, source_lang, target_lang)

if __name__ == '__main__':
    main()

