__author__ = 'bas'

import codecs
import gensim  # For testing
import os
from itertools import product

class UnavailableCorpusType(Exception):
    def __str__(self):
        return "Corpus type has to be in the following list: {}".format(
            ", ".join(Corpus.CORPUS_TYPES))


class UnavailableLanguageType(Exception):
    def __str__(self):
        return "Languages have to be in the following list: {}".format(
            ", ".join(Corpus.LANGUAGES))


class Corpus(object):

    CORPUS_TYPES = ["europarl", "training", "tuning", "word2vec"]
    LANGUAGES = ["en", "de"]

    CACHE_DIRECTORY = "model/cache"

    corpus_paths = {"europarl"	: "data/europarl-v7.de-en.%s",
                    "training"	: "data/news.2010.%s.shuffled",
                    "tuning"	: "data/news-commentary-v10.de-en.%s",
                    "word2vec"	: "data/newstest2010.%s"}

    def __init__(self, corpus_type='training', language='en', n_limit=None, use_phrases=True, cache=True):

        if corpus_type not in Corpus.CORPUS_TYPES:
            raise UnavailableCorpusType()

        if language not in Corpus.LANGUAGES:
            raise UnavailableLanguageType()

        self.language = language
        self.n_limit = n_limit
        self.corpus_type = corpus_type
        self.use_phrases = use_phrases
        self.cache = cache

        if self.cache:
            self._cache()

    def __iter__(self):
        path = Corpus.corpus_paths[self.corpus_type] % self.language
        path = os.path.abspath(os.path.join(os.curdir, path))

        for i, line in enumerate(codecs.open(path)):
            if self.n_limit is not None and i >= self.n_limit:
                break
            yield line.lower().split()

    def word2vec(self):
        if self.cache:
            return self._cache()
        else:
            return self._make_word2vec_model()

    def _cache(self):
        if not os.path.isdir(Corpus.CACHE_DIRECTORY):
            os.makedirs(Corpus.CACHE_DIRECTORY)

        limit = self.n_limit
        if limit is None:
            limit = -1

        cache_file_path = "%s/%s_%s_%d_%d" % (Corpus.CACHE_DIRECTORY,
                                              self.corpus_type,
                                              self.language,
                                              limit,
                                              self.use_phrases)

        path = os.path.abspath(os.path.join(os.curdir, cache_file_path))

        if not os.path.isfile(path):
            model = self._make_word2vec_model()
            model.save(path)
            return model
        else:
            return gensim.models.Word2Vec.load(cache_file_path)

    def _make_word2vec_model(self):
        iterable_corpus = self
        if self.use_phrases:
            bigram_transformed = gensim.models.Phrases(self)
            iterable_corpus = bigram_transformed[self]
        # Train 2 word2vec model
        model = gensim.models.Word2Vec(iterable_corpus, min_count=10, workers=8)
        model.init_sims()
        return model

class EuroparlCorpus(Corpus):

    def __init__(self, language='en', n_limit=None):
        super(EuroparlCorpus, self).__init__(corpus_type='europarl', language=language, n_limit=n_limit)


def test():
    print "Began Corpus() tests"

    limit = 10000
    print "Corpus limit: '%d'" % limit

    # test initialization and word2vec()
    for c_type, lang, lim in product(Corpus.CORPUS_TYPES, Corpus.LANGUAGES, [limit]):
        corpus = Corpus(corpus_type=c_type, language=lang, n_limit=lim)
        corpus.word2vec()

    # test errors
    try:
        _ = Corpus(corpus_type="ILoveBas", language="de", n_limit=limit)
    except UnavailableCorpusType:
        print "\nSuccessfully caught 'UnavailableCorpusType' exception"

    try:
        _ = Corpus(corpus_type="europarl", language="nl", n_limit=limit)
    except UnavailableLanguageType:
        print "\nSuccessfully caught 'UnavailableLanguageType' exception"

    print "\nFinished Corpus() tests successfully"


if __name__ == '__main__':
    test()
