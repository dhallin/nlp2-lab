from __future__ import division

import numpy as np
from scipy.spatial import distance

from translation_model import TranslationModel

__author__ = 'bas'


def run(l_source='de', l_target='en', corpus_limit=None):
    t_left = TranslationModel(l_source, l_target, dump_load=True)
    t_right = TranslationModel(l_target, l_source, dump_load=True)

    w_left = t_left.weights
    w_right = t_right.weights

    # Take the inverse of the translation models
    w_left_p = np.linalg.inv(w_left)
    w_right_p = np.linalg.inv(w_right)

    # Compute first and second order differences of the two matrices (element wise)
    diff_1_left = np.abs(w_right - w_left_p).sum().sum() / (w_right.shape[0] * w_right.shape[1])
    diff_1_right = np.abs(w_left - w_right_p).sum().sum() / (w_right.shape[0] * w_right.shape[1])
    diff_2_left = np.power(w_right - w_left_p, 2).sum().sum() / (w_right.shape[0] * w_right.shape[1])
    diff_2_right = np.power(w_left - w_right_p, 2).sum().sum() / (w_right.shape[0] * w_right.shape[1])

    print "First order diff:", diff_1_left, diff_1_right
    print "Second order diff:", diff_2_left, diff_2_right

    # Now compute differences between transformed vectors
    # c_source = Corpus(language=l_source, n_limit=corpus_limit)
    # c_target = Corpus(language=l_target, n_limit=corpus_limit)

    m_source = t_left.source_model
    m_target =  t_left.target_model

    vecs_source = m_source.syn0
    vecs_target = m_target.syn0

    vec_trans_source = np.dot(w_right, vecs_target.T).T
    vec_trans_source_p = np.dot(w_left_p, vecs_target.T).T

    vec_trans_target = np.dot(w_left, vecs_source.T).T
    vec_trans_target_p = np.dot(w_right_p, vecs_source.T).T

    diff_left = mse(vec_trans_source, vec_trans_source_p)
    diff_right = mse(vec_trans_target, vec_trans_target_p)
    print "MSE of target transformations", diff_left, diff_right

    # Now compute the P(5) for each word in the dictionary <- start here daniel
    print t_left.source_idxs, t_left.target_idxs
    # TODO: This doesn't seem to work! ^^
    dict_vec_source = m_source.syn0[t_left.source_idxs][0]
    dict_vec_target = m_target.syn0[t_left.target_idxs][0]

    # First from source to target
    est_vec_target = np.dot(w_left, dict_vec_source.T)
    est_vec_target_p = np.dot(w_right_p, dict_vec_source.T)
    print est_vec_target.shape, dict_vec_target.shape

    # These two values should be similar # TODO : fix
    print mse(est_vec_target, dict_vec_target), mse(est_vec_target_p, dict_vec_target)


def mse(A, B):
    # return np.sum(np.array([distance.norm(A[i] - B[i], ord=2) for i in range(len(A))])) / A.shape[0]
    return (np.array([distance.euclidean(A[i], B[i]) for i in range(len(A))])).mean()


if __name__ == '__main__':
    run()
