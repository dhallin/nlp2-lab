NLP2 - Lab 1 
============

Implementation of **IBM Model 1** & **2**.

## Literature

[Michael Collins lecture notes](http://www.cs.columbia.edu/~mcollins/courses/nlp2011/notes/ibm12.pdf "Michael Collins lecture notes")

## Tips

### Packages
`python counter`

### General
TATip: Map strings to integers. 


## Dataset:
http://www.statmt.org/wmt15/translation-task.html

- training: News Commentary v10
- tuning:  http://www.statmt.org/wmt12/dev.tgz (link kwam ik tegen in manual)
- word2vec models: news commentatry v10 en News Crawl: articles from 2010