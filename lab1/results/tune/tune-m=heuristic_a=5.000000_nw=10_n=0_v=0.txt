
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.2698  
   Recall    = 0.5186
   F-measure = 0.3549
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.4007
   Recall    = 0.1783
   F-measure = 0.2468
-----------------------------------
   AER       = 0.5589
