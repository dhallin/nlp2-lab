
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.2811  
   Recall    = 0.5404
   F-measure = 0.3699
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.4085
   Recall    = 0.1818
   F-measure = 0.2516
-----------------------------------
   AER       = 0.5464
