
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.1193  
   Recall    = 0.2293
   F-measure = 0.1570
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.1879
   Recall    = 0.0836
   F-measure = 0.1157
-----------------------------------
   AER       = 0.7979
