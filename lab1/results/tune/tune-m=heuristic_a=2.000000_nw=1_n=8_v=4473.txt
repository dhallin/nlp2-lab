
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.1268  
   Recall    = 0.2437
   F-measure = 0.1668
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.1970
   Recall    = 0.0877
   F-measure = 0.1214
-----------------------------------
   AER       = 0.7870
