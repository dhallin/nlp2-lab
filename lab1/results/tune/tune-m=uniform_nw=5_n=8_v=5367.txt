
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0460  
   Recall    = 0.0884
   F-measure = 0.0605
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0884
   Recall    = 0.0393
   F-measure = 0.0544
-----------------------------------
   AER       = 0.9116
