
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0345  
   Recall    = 0.0664
   F-measure = 0.0454
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0683
   Recall    = 0.0304
   F-measure = 0.0421
-----------------------------------
   AER       = 0.9324
