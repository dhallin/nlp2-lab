
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.1296  
   Recall    = 0.2491
   F-measure = 0.1705
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.1986
   Recall    = 0.0884
   F-measure = 0.1223
-----------------------------------
   AER       = 0.7841
