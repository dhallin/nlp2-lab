
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0445  
   Recall    = 0.0854
   F-measure = 0.0585
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0870
   Recall    = 0.0387
   F-measure = 0.0536
-----------------------------------
   AER       = 0.9136
