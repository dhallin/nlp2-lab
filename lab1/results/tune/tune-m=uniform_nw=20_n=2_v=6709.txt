
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0255  
   Recall    = 0.0490
   F-measure = 0.0336
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0560
   Recall    = 0.0249
   F-measure = 0.0345
-----------------------------------
   AER       = 0.9464
