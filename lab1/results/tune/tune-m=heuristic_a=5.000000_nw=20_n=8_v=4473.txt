
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0414  
   Recall    = 0.0795
   F-measure = 0.0544
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0796
   Recall    = 0.0354
   F-measure = 0.0490
-----------------------------------
   AER       = 0.9204
