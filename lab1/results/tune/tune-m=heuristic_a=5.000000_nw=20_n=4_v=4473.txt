
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0401  
   Recall    = 0.0770
   F-measure = 0.0527
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0781
   Recall    = 0.0348
   F-measure = 0.0481
-----------------------------------
   AER       = 0.9223
