
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0388  
   Recall    = 0.0745
   F-measure = 0.0510
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0764
   Recall    = 0.0340
   F-measure = 0.0471
-----------------------------------
   AER       = 0.9242
