
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0378  
   Recall    = 0.0726
   F-measure = 0.0497
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0773
   Recall    = 0.0344
   F-measure = 0.0476
-----------------------------------
   AER       = 0.9243
