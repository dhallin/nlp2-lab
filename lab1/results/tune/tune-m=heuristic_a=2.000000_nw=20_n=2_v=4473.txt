
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0393  
   Recall    = 0.0755
   F-measure = 0.0517
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0780
   Recall    = 0.0347
   F-measure = 0.0480
-----------------------------------
   AER       = 0.9229
