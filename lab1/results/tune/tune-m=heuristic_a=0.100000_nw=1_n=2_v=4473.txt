
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.1397  
   Recall    = 0.2684
   F-measure = 0.1837
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.2131
   Recall    = 0.0949
   F-measure = 0.1313
-----------------------------------
   AER       = 0.7679
