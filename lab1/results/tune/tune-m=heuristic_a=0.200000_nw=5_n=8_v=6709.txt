
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.0384  
   Recall    = 0.0738
   F-measure = 0.0505
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.0764
   Recall    = 0.0340
   F-measure = 0.0471
-----------------------------------
   AER       = 0.9245
