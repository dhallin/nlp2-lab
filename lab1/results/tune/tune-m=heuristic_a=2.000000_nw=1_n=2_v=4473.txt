
The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.1389  
   Recall    = 0.2670
   F-measure = 0.1827
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.2107
   Recall    = 0.0938
   F-measure = 0.1298
-----------------------------------
   AER       = 0.7701
