Data loaded: train-set: 50447 pairs, test-set: 447 pairs
Source vocabulary size: 19249
Target vocabulary size: 24925
Log-likelihood: -38558.574759	update: 38141.307018
Log-likelihood: -33224.883652	update: 5333.691106
Log-likelihood: -29744.613193	update: 3480.270459
Log-likelihood: -27852.718025	update: 1891.895168
Log-likelihood: -26949.746854	update: 902.971171
Log-likelihood: -26470.490321	update: 479.256533
Log-likelihood: -26195.930138	update: 274.560183
Log-likelihood: -26018.688333	update: 177.241805
Log-likelihood: -25892.026352	update: 126.661981
Log-likelihood: -25799.331086	update: 92.695266
Log-likelihood: -25726.972828	update: 72.358258
Log-likelihood: -25669.702631	update: 57.270197
Log-likelihood: -25626.152672	update: 43.549960
Log-likelihood: -25593.067881	update: 33.084790
Log-likelihood: -25568.812382	update: 24.255499
Log-likelihood: -25548.259213	update: 20.553169
Log-likelihood: -25529.377993	update: 18.881220
Log-likelihood: -25513.108308	update: 16.269686
Log-likelihood: -25498.905369	update: 14.202939
Log-likelihood: -25486.099830	update: 12.805538

The following results assume correct file formats.
Make sure you have previously checked the file format
using wa_check_submission.pl


    Word Alignment Evaluation   
----------------------------------
   Evaluation of SURE alignments 
   Precision = 0.4140  
   Recall    = 0.7957
   F-measure = 0.5446
-----------------------------------
   Evaluation of PROBABLE alignments
   Precision = 0.6560
   Recall    = 0.2919
   F-measure = 0.4041
-----------------------------------
   AER       = 0.2962
