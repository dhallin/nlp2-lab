#!/usr/bin/env bash
source ~/virtualenvs/nlp/bin/activate
chmod +x scripts/*
mkdir output
rm output/align-$1_$3.out
echo $1 > results/$1_$3.txt
python -u src/$1.py $2 $3 $4 $5 $6 2>&1 | tee results/$1_$3.txt
scripts/wa_eval_align.pl $2/test.wa.nonullalign output/align-$1_$3.out >> results/$1_$3.txt