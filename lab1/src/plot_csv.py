
__author__ = 'bas'

import matplotlib.pyplot as plt
import csv
from collections import defaultdict

def plot_likelihood():
    # create plots
    colors = ["r", "b", "g", "k", "m"]
    plt.figure(figsize=(15, 10))
    csv_files = [("lexical", "output/loglike-runscript_lexical.csv"),
                 ("random-1", "output/loglike-runscript_random-1.csv"),
                 ("random-2", "output/loglike-runscript_random-2.csv"),
                 ("random-3", "output/loglike-runscript_random-3.csv"),
                 ("uniform", "output/loglike-runscript_uniform.csv")]
    output_name = "_".join([name for name, path in csv_files])
    log_likes = defaultdict(list)
    for name, path in csv_files:
        with open(path, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                log_likes[name].append(row[1])
        plt.plot(log_likes[name], label="Log Likelihood for %s" % name)
    plt.title("Log likelihood per iteration")
    plt.legend(loc="lower right")
    plt.xlabel("Iteration")
    plt.ylabel("Log Likelihood")

    plt.savefig("output/%s_loglike.pdf" % output_name)

if __name__ == '__main__':
    plot_likelihood()