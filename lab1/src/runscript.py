import argparse

__author__ = 'Daniel'

import sys
from os import path
import os

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from data.data import Data

from src.IBM1 import IBM1
from src.IBM2 import IBM2

if __name__ == "__main__":
    ## PARAMETERS ##
    sentence_limit = 50000
    update_threshold = 1
    iteration_limit = 20

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('data_prefix', type=str, default='data/', nargs='?')
    parser.add_argument('init_method', type=str, default='data/', nargs='?')
    args = parser.parse_args()

    _data = Data(limit=sentence_limit, add_null_word=True, source_path='%s/hansards.36.2.e' % args.data_prefix,
                 test_source_path='%s/test.e' % args.data_prefix, target_path='%s/hansards.36.2.f' % args.data_prefix,
                 test_target_path='%s/test.f' % args.data_prefix)
    ibm1 = IBM2(init_method=args.init_method) # uniform, random, lexical

    t_model, q_model, log_likes = ibm1.align(_data, update_threshold, iteration_limit)
    # ibm1.plot_likelihood(log_likes)

    output = ibm1.viterbi_align(t_model, q_model, _data)  # q_model, _data)

    with open('output/align-%s_%s.out' % (os.path.basename(__file__).split('.')[0], args.init_method), 'w') as out:
        for row in output:
            out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))
    with open('output/loglike-%s_%s.csv' % (os.path.basename(__file__).split('.')[0], args.init_method), 'w') as out:
        for i, ll in enumerate(log_likes):
            out.write("%d, %s \n" % (i, str(ll)))