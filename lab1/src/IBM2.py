__author__ = 'Daniel'

import sys
from os import path
import os

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from data.data import Data

import numpy as np
from numpy import sum, log as ln
from itertools import product
from collections import defaultdict
from functools import partial
from src.IBM1 import IBM1, IBMAbstract
import argparse
import gc
import sys
import __builtin__
try:
    __builtin__.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func):
        return func


class IBM2(IBMAbstract):
    @profile
    def __init__(self, init_method="uniform"):
        super(IBM2, self).__init__()
        self.init_method = init_method

    @profile
    def initialize_t_model(self, s_length, t_length, data=None):
        if self.init_method == 'uniform':
            t_model = np.ones((s_length, t_length),dtype=np.float32) / float(t_length)  # init translation model
        elif self.init_method == 'random':
            rand_init = np.random.rand(s_length, t_length).astype(np.float32)
            t_model = np.divide(rand_init, np.sum(rand_init, axis=0))
            # assert all(np.sum(t_model, axis=0))
        elif self.init_method == 'lexical':
            ibm1 = IBM1()
            t_model, _ = ibm1.align(data, 2,20)

        # assert all(np.sum(t_model, axis=0)) == 1
        return t_model

    def init_q_model_entry(self, sl, tl):
        if self.init_method == 'uniform' or self.init_method == 'lexical':
            return np.ones((sl, tl), dtype=np.float32) / float(sl)
        elif self.init_method == 'random':
            rand_init = np.random.rand(sl, tl).astype(np.float32)
            return np.divide(rand_init, np.sum(rand_init, axis=0, keepdims=1))

    @profile
    def initialize_q_model(self, s_ml, t_ml):
        q_model = dict()
        if self.init_method == 'uniform' or self.init_method == 'lexical':
            for (sl, tl) in product(range(1, s_ml + 1), range(t_ml + 1)):
                self.init_q_model_entry(q_model, sl, tl)
                # assert all(np.sum(q_model[sl, tl], axis=0))
        elif self.init_method == 'random':
            for (sl, tl) in product(range(1, s_ml + 1), range(t_ml + 1)):
                rand_init = np.random.rand(sl, tl).astype(np.float32)
                q_model[sl, tl] = np.divide(rand_init, np.sum(rand_init, axis=0, keepdims=1))
                # assert all(np.sum(q_model[sl, tl], axis=0))
                # q_model[sl, tl] = np.ones((sl, tl)) / float(sl)
            pass
        return q_model



    @profile
    def align(self, data, threshold, iteration_limit):
        log_likes = []
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)
        # s_ml = data.source_vocab_max_length
        # t_ml = data.target_vocab_max_length

        t_model = self.initialize_t_model(s_length, t_length, data)

        q_model = dict() #self.initialize_q_model(s_ml, t_ml)

        log_likelihood = self.log_likelihood(t_model, q_model, data)
        update = sys.float_info.max

        iter = 0
        while update > threshold and iter < iteration_limit:

            # e-step
            t_counts = np.zeros((s_length, t_length), dtype=np.float32)  # declare counts
            q_counts = dict()

            for (ss, ts) in data.sentence_pairs_idxs:
                sl, tl = len(ss), len(ts)  # sentence lengths
                st = t_model[np.ix_(ss, ts)]  # sentence translation model
                if (sl, tl) not in q_model:
                    q_model[sl, tl] = self.init_q_model_entry(sl, tl)
                sq = q_model[sl, tl]  # sentence alignment model


                delta = np.multiply(st, sq)  # compute delta
                norm_delta = np.divide(delta, sum(delta, axis=0, keepdims=True))  # normalize delta
                if q_counts.has_key((sl, tl)):
                    q_counts[sl, tl] += norm_delta
                else:
                    q_counts[sl, tl] = norm_delta

                for s_idx, sw in enumerate(ss):
                    for t_idx, tw in enumerate(ts):
                        t_counts[sw, tw] += norm_delta[s_idx, t_idx]
            # m-step
            t_model = np.divide(t_counts, sum(t_counts, axis=1, keepdims=True))  # normalized t-counts
            for sl, tl in q_counts.keys():
                q_model[sl, tl] = np.divide(q_counts[sl, tl],
                                            sum(q_counts[sl, tl], axis=1, keepdims=True))  # normalized q-counts

            # compute likelihood
            old_log_likelihood = log_likelihood
            log_likelihood = self.log_likelihood(t_model, q_model, data)
            log_likes.append(log_likelihood)

            update = abs(old_log_likelihood - log_likelihood)

            print "Log-likelihood: %f\tupdate: %f" % (log_likelihood, update)
            sys.stdout.flush()
            iter += 1
            gc.collect()
        return t_model, q_model, log_likes


    @profile
    def log_likelihood(self, t_model, q_model, data):
        # TODO: likelihood stops converging at iteration ~6 and goes up. Float error?

        ll = 0
        epsilon = 1

        for (ss, ts) in data.test_sentence_pairs_idxs:
            sl, tl = len(ss), len(ts)  # sentence lengths
            st = t_model[np.ix_(ss, ts)]  # sentence translation model
            if (sl, tl) not in q_model:
                sq = self.init_q_model_entry(sl, tl)
            else:
                sq = q_model[sl, tl]  # sentence alignment model
            stq = np.multiply(st, sq)  # relative translation model
            ll += ln(epsilon) + sum(ln(sum(stq, axis=0)))  # add to log-likelihood
        return ll 

    @profile
    def viterbi_align(self, t_model, q_model, data):
        translation_list = []
        lexicon = [(t, s) for t, s in enumerate(np.argmax(t_model, axis=0))]

        for si, pair in enumerate(data.test_sentence_pairs_idxs):
            ss, ts = pair
            for twi, tw in enumerate(ts):
                # find the most likely word given the sentence
                max_p = -1
                max_sw = -1
                for swi,j in enumerate(ss):
                    cur_prob = t_model[j, tw] * q_model[len(ss), len(ts)][swi, twi] # TODO: check if this is correct
                    if cur_prob > max_p:
                        max_p = cur_prob
                        max_sw = j

                assert max_sw > -1, "should have found a word"

                swi = ss.index(max_sw)
                translation_list.append((si, swi, twi))
            gc.collect()
        return translation_list

if __name__ == "__main__":

    sentence_limit = None  # 10000
    update_threshold = 1
    iteration_limit = 20
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('data_prefix', type=str, default='data/', nargs='?')
    args = parser.parse_args()

    _data = Data(limit=sentence_limit, add_null_word=True, source_path='%s/hansards.36.2.e' % args.data_prefix,
                 test_source_path='%s/test.e' % args.data_prefix, target_path='%s/hansards.36.2.f' % args.data_prefix,
                 test_target_path='%s/test.f' % args.data_prefix)
    ibm2 = IBM2(init_method='uniform') # uniform, random, lexical
    #  TODO: fix random

    t_model, q_model, log_likes = ibm2.align(_data, update_threshold, iteration_limit=iteration_limit)
    ibm2.plot_likelihood(log_likes)
    lexicon = [(s, t) for s, t in enumerate(np.argmax(t_model, axis=1))]
    confidence = [c for c in np.max(t_model, axis=1)[:-1]]

    lexicon = [(_data.source_vocab[s], _data.target_vocab[t]) for _, (s, t) in
               sorted(zip(confidence, lexicon), reverse=True)]
    print "\n"
    print lexicon[:20]
    print lexicon[-20:]

    output = ibm2.viterbi_align(t_model, q_model, _data)

    # lexicon = [(t, s) for t, s in enumerate(np.argmax(model, axis=0))]
    # confidence = [c for c in np.max(model, axis=0)]

    # lexicon = [(_data.source_vocab[t], _data.target_vocab[s])
    # for _, (t, s) in sorted(zip(confidence, lexicon), reverse=True)]
    with open('output/%s.out' % os.path.basename(__file__).split('.')[0], 'w') as out:
        # csv_out.writerow(['name', 'num'])
        for row in output:
            out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))
    with open('output/%s.loglike.out' % os.path.basename(__file__).split('.')[0], 'w') as out:
        for row in output:
            out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))

    for si, swi, twi in output[0:100]:
        print "Sentence #%d: %s -> %s " % (si, _data.source_vocab[_data.sentence_pairs_idxs[si][0][swi]],
                                           _data.target_vocab[_data.sentence_pairs_idxs[si][1][twi]], )

    print _data.sentence_pairs[0:10]