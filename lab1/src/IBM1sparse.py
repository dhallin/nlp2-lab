import argparse

__author__ = 'Daniel'

import sys
from os import path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from data.data import Data
import os
from collections import defaultdict

import numpy as np
from numpy import log as ln, bincount as cnt
from numpy import sum
import itertools
import matplotlib.pyplot as plt
# from memory_profiler import profile
from scipy import sparse as sp
from sklearn.preprocessing import normalize

def profile(func):
    return func

class IBMAbstract(object):
    @profile
    def __init__(self):
        pass

    @profile
    def plot_likelihood(self, log_likes):
        # create plots
        colors = ["r", "b", "g", "k", "m"]
        plt.figure(figsize=(15, 10))

        plt.plot(log_likes, c=colors[0], label="Log Likelihood")
        plt.title("Log likelihood per iteration")
        plt.legend(loc="upper left")
        plt.xlabel("Iteration")
        plt.ylabel("Log Likelihood")
        plt.savefig("output/%s_loglike.pdf" % self.__class__.__name__)


class IBM1(IBMAbstract):


    @profile
    def for_loop_align(self, data, iterations=10):
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)
        t_model = np.ones((s_length, t_length)) / float(t_length)  # init translation model

        for _ in range(iterations):
            count_ef = defaultdict(float)
            total_f = defaultdict(float)

            for e_s, f_s in data.sentence_pairs_idxs:
                total_s = defaultdict(float)

                for e_w in e_s:
                    for f_w in f_s:
                        total_s[e_w] += t_model[e_w, f_w]  # normalization constant, sum over e
                for e_w in e_s:
                    for f_w in f_s:
                        count_ef[e_w, f_w] += t_model[e_w, f_w] / total_s[e_w]
                        total_f[f_w] += t_model[e_w, f_w] / total_s[e_w]
            for f_w in range(t_length):
                for e_w in range(s_length + 1):
                    t_model[e_w, f_w] = count_ef[e_w, f_w] / total_f[f_w]

        return t_model

    @profile
    def align(self, data, threshold):
        # TODO: Why does this grow again?
        log_likes = []
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)

        t_model = np.ones((s_length, t_length)) / float(t_length)  # init translation model

        log_likelihood = self.log_likelihood(t_model, data)
        update = threshold + 1 #sys.float_info.max

        while update > threshold:

            # e-step
            # counts = np.zeros((s_length, t_length))  # declare counts
            counts = sp.csc_matrix((s_length, t_length))
            for (ss, ts) in data.sentence_pairs_idxs:
                uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes

                sc = np.outer(cnt(ss)[uss], cnt(ts)[uts])  # sentence counts
                st = t_model[np.ix_(uss, uts)]  # sentence translation model
                stc = sp.csc_matrix(np.multiply(sc, st))  # relative translation model
                # TODO: change axis 1 -> 0 in all models
                norm_stc = normalize(stc, norm='l1', axis=0) #np.divide(stc, np.sum(stc, axis=0, keepdims=True).astype(float))  # normalize for all source words
                assert np.all(norm_stc.sum(axis=0)) == 1.0, "should be normalized"
                counts[np.ix_(uss, uts)] += norm_stc  # add to total counts

            # m-step
            # TODO: change axis 0 -> 1 in all models
            t_model = normalize(counts, norm='l1', axis=1)  # normalized counts
            assert np.all(np.sum(t_model, axis=1)) == 1.0, "should be normalized"
            # compute likelihood
            old_log_likelihood = log_likelihood
            log_likelihood = self.log_likelihood(t_model, data)
            log_likes.append(log_likelihood)
            update = abs(old_log_likelihood - log_likelihood)

            print "\rLog-likelihood: %f\tupdate: %f" % (log_likelihood, update)

        return t_model,log_likes

    @profile
    def log_likelihood(self, t_model, data):
        # TODO: likelihood stops converging at iteration ~6 and goes up. Float error?

        nw_idx = len(data.source_vocab)
        ll = 0
        epsilon = 1

        for (ss, ts) in data.sentence_pairs_idxs:
            sl, tl = len(ss), len(ts)
            uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes

            sc = np.outer(cnt(ss)[uss], cnt(ts)[uts])  # sentence counts
            st = t_model[np.ix_(uss, uts)]  # sentence translation model
            stc = np.multiply(sc, st)  # relative translation model
            ll += ln(epsilon) - tl * ln(sl) + sum(ln(sum(stc, axis=0)))
        return ll

    @profile
    def viterbi_align(self, model, data):
        translation_list = []
        lexicon = [(t, s) for t, s in enumerate(np.argmax(model, axis=0))]

        for si, pair in enumerate(data.test_sentence_pairs_idxs):
            ss, ts = pair
            # uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes
            # # st_model = model[np.ix_(uss, uts)]  # sentence translation model
            # st_model = [(sw, tw) for (sw, tw) in model if sw in ss]
            for twi, tw in enumerate(ts):
                # find the most likely word given the sentence
                max_p = -1
                max_sw = -1
                for j in ss:
                    if model[j,tw] > max_p:
                        max_p = model[j, tw]
                        max_sw = j

                assert max_sw > -1, "should have found a word"

                swi = ss.index(max_sw)
                translation_list.append((si, swi, twi))
                # sentenceNum foreignWordPosition englishWordPosition
        return translation_list



if __name__ == "__main__":
    # s = english, t = dutch/french
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('data_prefix', type=str, default='data/', nargs='?')
    args = parser.parse_args()

    _data = Data(add_null_word=True, source_path='%s/test.e' % args.data_prefix, target_path='%s/test.f' % args.data_prefix)
    ibm1 = IBM1()

    model, log_likes = ibm1.align(_data, 1000)
    ibm1.plot_likelihood(log_likes)
    output = ibm1.viterbi_align(model, _data)

    # lexicon = [(t, s) for t, s in enumerate(np.argmax(model, axis=0))]
    # confidence = [c for c in np.max(model, axis=0)]

    # lexicon = [(_data.source_vocab[t], _data.target_vocab[s])
    # for _, (t, s) in sorted(zip(confidence, lexicon), reverse=True)]
    with open('output/%s.out' % os.path.basename(__file__).split('.')[0], 'w') as out:
        # csv_out.writerow(['name', 'num'])
        for row in output:
            # TODO: Fix this +1 everywhere (also model 2)
            out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))

    for si, swi, twi in output[30:60]:
        print "Sentence #%d: %s -> %s " % (si, _data.source_vocab[_data.sentence_pairs_idxs[si][0][swi]],
                                           _data.target_vocab[_data.sentence_pairs_idxs[si][1][twi]], )

    # print output[:10]
    # print output[-10:]