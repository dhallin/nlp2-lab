import argparse

__author__ = 'Daniel'

import sys
from os import path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from data.data import Data
import os
from collections import defaultdict

import numpy as np
npa = np.array
from numpy import log as ln, bincount as cnt
from numpy import sum
import itertools
import matplotlib.pyplot as plt

from IBM1 import IBM1


class IBM1Moore(IBM1):

    # Added smoothing from Moore-2004
    def align(self, data, iterations, init_method="uniform", n=10, v_len=3500, nullwnr=10, alpha=2.0, llr=None):
        log_likes = []
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)

        t_model = None
        if init_method == "uniform":
            t_model = np.ones((s_length, t_length)) / float(t_length)  # init translation model
        elif init_method == "heuristic":
            if llr is None:
                llr = self.llr(data, nullwnr, alpha)
            t_model = llr
        else:
            assert False, "Invalid parameter"

        log_likelihood = self.log_likelihood(t_model, data)
        update = sys.float_info.max

        i = 0
        while i < iterations:

            # e-step
            counts = np.zeros((s_length, t_length))  # declare counts

            for (ss, ts) in data.sentence_pairs_idxs:
                uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes

                sc = np.outer(cnt(ss)[uss], cnt(ts)[uts])  # sentence counts
                st = t_model[np.ix_(uss, uts)]  # sentence translation model
                stc = np.multiply(sc, st)  # relative translation model

                stc[-1, :] *= nullwnr

                norm_stc = np.divide(stc, np.sum(stc, axis=0, keepdims=True))  # normalize for all source words

                counts[np.ix_(uss, uts)] += norm_stc  # add to total counts

            # m-step - Moore
            t_model = np.divide(counts + n, np.sum(counts,
                                axis=1, keepdims=True) + n * v_len)  # normalized smoothed counts

            # compute likelihood
            old_log_likelihood = log_likelihood
            log_likelihood = self.log_likelihood(t_model, data)
            log_likes.append(log_likelihood)
            update = abs(old_log_likelihood - log_likelihood)

            print "\r%d. Log-likelihood: %f\tupdate: %f" % (i, log_likelihood, update)
            i += 1
        return t_model, log_likes

    def llr_multiple(self, data, nullwnrs, alphas):
        llrs = dict()
        print "Started computing LLRs"
        unormalized_llr, counts = self.unnorm_llr(data)
        for nullwnr, alpha in itertools.product(nullwnrs, alphas):
            llrs[alpha, nullwnr] = self.normalize_llr(unormalized_llr, counts, nullwnr, alpha)
        print "Finished computing LLRs"
        return llrs

    def llr(self, data, nullwnr, alpha):
        print "Started computing LLR"
        unormalized_llr, counts = self.unnorm_llr(data)
        llr = self.normalize_llr(unormalized_llr, counts, nullwnr, alpha)
        print "Finished computing LLR"
        return llr

    def normalize_llr(self, llr, counts, nullwnr, alpha):

        # compute p(t) and set null-word to this
        target_counts = np.sum(counts[:, :, :, 1], axis=(0, 2))
        p_t = np.divide(target_counts, np.sum(target_counts).astype(float))
        llr[-1, :] = p_t * nullwnr

        # normalize with maximum value
        normalize_constant = np.max(np.sum(llr, axis=0))  # normalize constant is the largest of the sums over target words.
        total_llr = np.divide(llr, normalize_constant)
        total_llr = np.power(total_llr, alpha)
        total_llr = np.divide(total_llr, np.sum(total_llr, axis=1, keepdims=1))

        return total_llr

    def unnorm_llr(self, data):
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)

        total_counts = np.zeros((s_length, t_length, 2, 2))  # source word, target word, sw present, tw present [0,1]

        for (ss, ts) in data.sentence_pairs_idxs:
            total_counts[:, :, 0, 0] += 1                            # all: add counts for non present
            total_counts[ss, :, :, 0] += [-1, 1]                     # correct non present and add for single present
            total_counts[:, ts, 0, :] += [-1, 1]                     # correct non present and add for single present
            total_counts[np.ix_(ss, ts)] += npa([[0, -1], [-1, 1]])  # correct single present and add for double present

        total_llr = np.zeros((s_length, t_length))                   # declare counts

        for sw in range(s_length):
            for tw in range(t_length):
                counts = total_counts[sw, tw, :, :]

                if counts[1, 1] > 0:  # Will always be true if next condition is true.
                    p_s = np.divide(np.sum(counts, axis=1), np.sum(counts).astype(float))  # [p(!s), p(s)]
                    p_t = np.divide(np.sum(counts, axis=0), np.sum(counts).astype(float))  # [p(!t), p(t)]

                    p_st = np.divide(counts, np.sum(counts).astype(float))           # joint probability
                    p_t_given_s = np.divide(p_st, np.array([p_s]).T)  # TODO: Break these equations out of the for-loop

                    if p_st[1, 1] > p_s[1] * p_t[1]:  # TODO: Check if this is what the paper means.
                        for swp, twp in itertools.product([0, 1], [0, 1]):
                            if counts[swp, twp]:  # To avoid nan results
                                total_llr[sw, tw] += counts[swp, twp] * np.log(np.divide(p_t_given_s[swp, twp], float(p_t[twp])))  # TODO: Rewrite with nan_to_num
        return total_llr, total_counts

    def tune_parameters(self):
        data = Data(add_null_word=True, source_path='data/hansards.36.2.e', target_path='data/hansards.36.2.f', test_source_path='data/test.e',
                 test_target_path='data/test.f', limit=1000)

        voclen = len(data.target_vocab)

        alphas =   [0.2, 0.5, 1.0, 2.0, 5.0]
        nullwnrs = [1, 5, 10, 20]
        ns = [int((voclen / 1000) * factor) for factor in [0.5, 1, 2.0]]
        vs = [int(voclen * factor) for factor in [1.0, 1.2, 1.5]]

        nvs = [(0, 0)] + [(n, v) for n, v in itertools.product(ns, vs)]

        for (nullwnr, (n, v)) in itertools.product(nullwnrs, nvs):
            file_name = "tune-m=uniform_nw=%d_n=%d_v=%d.out" % (nullwnr, n, v)
            print file_name
            model, ll = self.align(data, 20, init_method="uniform", nullwnr=nullwnr, n=n, v_len=v)
            self.plot_likelihood(ll, output_name=file_name.split(".")[0])
            output = self.viterbi_align(model, data)

            with open('output/tune/%s' % file_name, 'w') as out:
                for row in output:
                    out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))

        llrs = self.llr_multiple(data, nullwnrs, alphas)

        for (alpha, nullwnr, (n, v)) in itertools.product(alphas, nullwnrs, nvs):

            file_name = "tune-m=heuristic_a=%f_nw=%d_n=%d_v=%d.out" % (alpha, nullwnr, n, v)
            print file_name
            llr = llrs[alpha, nullwnr]
            model, ll = self.align(data, 20, init_method="heuristic", nullwnr=nullwnr, n=n, v_len=v, alpha=alpha, llr=llr)
            self.plot_likelihood(ll, output_name=file_name.split(".")[0])
            output = self.viterbi_align(model, data)

            with open('output/tune/%s' % file_name, 'w') as out:
                for row in output:
                    out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))


def main():
    # best params n=0 v=0 nw=5 init=uniform
    sentence_limit = None
    iterations = 20
    ibm = IBM1Moore()

    name = '%s-it=%d-snr=%s' % (os.path.basename(__file__).split('.')[0], iterations, str(sentence_limit))

    data = Data(add_null_word=True, source_path='data/hansards.36.2.e', target_path='data/hansards.36.2.f', test_source_path='data/test.e',
                 test_target_path='data/test.f', limit=sentence_limit)
    m, ll = ibm.align(data, iterations, init_method="uniform", n=0, v_len=0, nullwnr=5)

    ibm.plot_likelihood(ll, output_name=name)
    output = ibm.viterbi_align(m, data)

    with open('output/%s.out' % name, 'w') as o:
        for r in output:
            o.write("%04d %d %d\n" % (r[0] + 1, r[1] + 1, r[2] + 1))

if __name__ == "__main__":

    main()
    print "Finished"
    sys.exit(0)

    # s = english, t = dutch
    # parser = argparse.ArgumentParser(description='Process some integers.')
    # parser.add_argument('data_prefix', type=str, default='data/', nargs='?')
    # args = parser.parse_args()

    _data = Data(add_null_word=True, source_path='%s/test.e' % args.data_prefix, target_path='%s/test.f' % args.data_prefix)
    ibm_moore = IBM1Moore()

    # ibm_moore.tune_parameters()
    #
    # sys.exit(0)
    model, log_likes = ibm_moore.align(_data, 100)

    ibm_moore.plot_likelihood(log_likes)
    output = ibm_moore.viterbi_align(model, _data)

    # lexicon = [(t, s) for t, s in enumerate(np.argmax(model, axis=0))]
    # confidence = [c for c in np.max(model, axis=0)]

    # lexicon = [(_data.source_vocab[t], _data.target_vocab[s])
    # for _, (t, s) in sorted(zip(confidence, lexicon), reverse=True)]
    with open('output/%s.out' % os.path.basename(__file__).split('.')[0], 'w') as out:
        # csv_out.writerow(['name', 'num'])
        for row in output:
            out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))

    for si, swi, twi in output[30:60]:
        print "Sentence #%d: %s -> %s " % (si, _data.source_vocab[_data.sentence_pairs_idxs[si][0][swi]],
                                           _data.target_vocab[_data.sentence_pairs_idxs[si][1][twi]], )

    # print output[:10]
    # print output[-10:]