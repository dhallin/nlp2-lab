__author__ = 'Daniel'
import subprocess
import os

def viterbi_alignment(s_idxs, t_idxs, translation_model):
    pass


def find_best(folder):
    file_AER_score = []
    file_precision_score = []
    file_recall_score = []
    file_f_score = []
    for i, filename in enumerate(os.listdir(folder)):
        print filename
        with open("%s/%s" % (folder, filename), 'r') as infile:
            precision, recall, fmeasure = False, False, False
            for line in infile:
                tokens = line.strip().split()
                if len(tokens) and tokens[0] == "Precision" and not precision:
                    file_precision_score.append((tokens[2], filename))
                    precision = True
                if len(tokens) and tokens[0] == "Recall" and not recall:
                    file_recall_score.append((tokens[2], filename))
                    recall = True
                if len(tokens) and tokens[0] == "AER":
                    file_AER_score.append((tokens[2], filename))
                if len(tokens) and tokens[0] == "F-measure" and not fmeasure:
                    file_f_score.append((tokens[2], filename))
                    fmeasure = True

    # file_AER_score.sort(key=lambda tup: tup[0])
    # file_recall_score.sort(key=lambda tup: tup[0], reverse=True)
    # file_precision_score.sort(key=lambda tup: tup[0], reverse=True)
    for i in range(len(file_AER_score)):
        print "%s p: %s r: %s f: %s aer: %s" % (file_AER_score[i][1],
                                                file_precision_score[i][0],
                                                file_recall_score[i][0],
                                                file_f_score[i][0],
                                                file_AER_score[i][0])
    # for score, file in file_AER_score[:10]:
    #     print "File: %s, \tAER score: %s" % (file, score)
    # for score, file in file_precision_score[:10]:
    #     print "File: %s, \tPrecision score: %s" % (file, score)
    # for score, file in file_recall_score[:10]:
    #     print "File: %s, \tRecall score: %s" % (file, score)


    print "Finished"

if __name__ == "__main__":
    find_best("./results/tune")
