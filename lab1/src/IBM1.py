import argparse

__author__ = 'Daniel'

import sys
from os import path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from data.data import Data
import os
from collections import defaultdict

import numpy as np
from numpy import log as ln, bincount as cnt
from numpy import sum
import itertools
import matplotlib.pyplot as plt

import scipy.sparse
import cPickle as pickle

# from memory_profiler import profile
from scipy import sparse as sp
from sklearn.preprocessing import normalize
import __builtin__
try:
    __builtin__.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func):
        return func

class IBMAbstract(object):
    @profile
    def __init__(self):
        pass

    @profile
    def plot_likelihood(self, log_likes, output_name=None):
        # create plots
        colors = ["r", "b", "g", "k", "m"]
        plt.figure(figsize=(15, 10))

        plt.plot(log_likes, c=colors[0], label="Log Likelihood")
        plt.title("Log likelihood per iteration")
        plt.legend(loc="upper left")
        plt.xlabel("Iteration")
        plt.ylabel("Log Likelihood")
        if not output_name:
            output_name = self.__class__.__name__

        plt.savefig("output/%s_loglike.pdf" % output_name)


class IBM1(IBMAbstract):


    @profile
    def for_loop_align(self, data, iterations=10):
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)
        t_model = np.ones((s_length, t_length)) / float(t_length)  # init translation model

        for _ in range(iterations):
            count_ef = defaultdict(float)
            total_f = defaultdict(float)

            for e_s, f_s in data.sentence_pairs_idxs:
                total_s = defaultdict(float)

                for e_w in e_s:
                    for f_w in f_s:
                        total_s[e_w] += t_model[e_w, f_w]  # normalization constant, sum over e
                for e_w in e_s:
                    for f_w in f_s:
                        count_ef[e_w, f_w] += t_model[e_w, f_w] / total_s[e_w]
                        total_f[f_w] += t_model[e_w, f_w] / total_s[e_w]
            for f_w in range(t_length):
                for e_w in range(s_length + 1):
                    t_model[e_w, f_w] = count_ef[e_w, f_w] / total_f[f_w]

        return t_model

    @profile
    def align(self, data, threshold, iteration_limit):
        # TODO: Why does this grow again?
        log_likes = []
        s_length = len(data.source_vocab)
        t_length = len(data.target_vocab)

        print "t_model initialized"
        t_model = np.ones((s_length, t_length),dtype=np.float32) / float(t_length)  # init translation model

        log_likelihood = self.log_likelihood(t_model, data)
        update = threshold + 1

        iter = 0
        # while update > threshold and iter < iteration_limit:
        while iter < iteration_limit:
            # e-step
            counts = np.zeros((s_length, t_length),dtype=np.float32)  # declare counts

            for (ss, ts) in data.sentence_pairs_idxs:
                uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes

                sc = np.outer(cnt(ss)[uss], cnt(ts)[uts])  # sentence counts
                st = t_model[np.ix_(uss, uts)]  # sentence translation model
                stc = np.multiply(sc, st)  # relative translation model
                norm_stc = np.divide(stc, np.sum(stc, axis=0, keepdims=True).astype(np.float32))  # normalize for all source words
                assert np.all(np.sum(norm_stc, axis=0)) == 1.0, "should be normalized"
                counts[np.ix_(uss, uts)] += norm_stc  # add to total counts

            # m-step
            t_model = np.divide(counts, np.sum(counts, axis=1, keepdims=True).astype(np.float32))  # normalized counts
            # t_model = np.divide(counts, counts.sum(axis=1).astype(float)) # normalize
            assert np.all(np.sum(t_model, axis=1)) == 1.0, "should be normalized"
            # compute likelihood
            old_log_likelihood = log_likelihood
            log_likelihood = self.log_likelihood(t_model, data)
            log_likes.append(log_likelihood)
            update = abs(old_log_likelihood - log_likelihood)

            print "\rLog-likelihood: %f\tupdate: %f" % (log_likelihood, update)
            iter += 1
        return t_model,log_likes

    @profile
    def log_likelihood(self, t_model, data):
        # TODO: likelihood stops converging at iteration ~6 and goes up. Float error?

        nw_idx = len(data.source_vocab)
        ll = 0
        epsilon = 1

        for (ss, ts) in data.sentence_pairs_idxs:
            sl, tl = len(ss), len(ts)
            uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes

            sc = np.outer(cnt(ss)[uss], cnt(ts)[uts])  # sentence counts
            st = t_model[np.ix_(uss, uts)]  # sentence translation model
            stc = np.multiply(sc, st)  # relative translation model
            ll += ln(epsilon) - tl * ln(sl) + sum(ln(sum(stc, axis=0)))
        return ll

    @profile
    def viterbi_align(self, model, data):
        translation_list = []
        lexicon = [(t, s) for t, s in enumerate(np.argmax(model, axis=0))]

        for si, pair in enumerate(data.test_sentence_pairs_idxs):
            ss, ts = pair
            # uss, uts = np.unique(ss), np.unique(ts)  # unique word indexes
            # # st_model = model[np.ix_(uss, uts)]  # sentence translation model
            # st_model = [(sw, tw) for (sw, tw) in model if sw in ss]
            for twi, tw in enumerate(ts):
                # find the most likely word given the sentence
                max_p = -1
                max_sw = -1
                for j in ss:
                    if model[j,tw] > max_p:
                        max_p = model[j, tw]
                        max_sw = j

                assert max_sw > -1, "should have found a word"

                swi = ss.index(max_sw)
                translation_list.append((si, swi, twi))
                # sentenceNum foreignWordPosition englishWordPosition
        return translation_list

def main():
    # best params n=0 v=0 nw=5 init=uniform
    sentence_limit = None
    iterations = 20
    ibm = IBM1()

    name = '%s-it=%d-snr=%s' % (os.path.basename(__file__).split('.')[0], iterations, str(sentence_limit))

    data = Data(add_null_word=True, source_path='data/hansards.36.2.e', target_path='data/hansards.36.2.f', test_source_path='data/test.e',
                 test_target_path='data/test.f', limit=sentence_limit)
    m, ll = ibm.align(data, 0, iterations)


    ibm.plot_likelihood(ll, output_name=name)
    output = ibm.viterbi_align(m, data)

    with open('output/%s.out' % name, 'w') as o:
        for r in output:
            o.write("%04d %d %d\n" % (r[0] + 1, r[1] + 1, r[2] + 1))

if __name__ == "__main__":
    # s = english, t = dutch/french
    # _data = Data(add_null_word=True, source_path='data/hansards.36.2.e', test_source_path='data/test.e', target_path='data/hansards.36.2.f', test_target_path='data/test.f')

    main()
    sys.exit(0)

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('data_prefix', type=str, default='data/', nargs='?')
    args = parser.parse_args()

    _data = Data(add_null_word=True, source_path='%s/test.e' % args.data_prefix, target_path='%s/test.f' % args.data_prefix)
    ibm1 = IBM1()

    model, log_likes = ibm1.align(_data, 100, None)
    ibm1.plot_likelihood(log_likes)
    output = ibm1.viterbi_align(model, _data)

    # lexicon = [(t, s) for t, s in enumerate(np.argmax(model, axis=0))]
    # confidence = [c for c in np.max(model, axis=0)]

    # lexicon = [(_data.source_vocab[t], _data.target_vocab[s])
    # for _, (t, s) in sorted(zip(confidence, lexicon), reverse=True)]
    with open('output/%s.out' % os.path.basename(__file__).split('.')[0], 'w') as out:
        # csv_out.writerow(['name', 'num'])
        for row in output:
            # TODO: Fix this +1 everywhere (also model 2)
            out.write("%04d %d %d\n" % (row[0] + 1, row[1] + 1, row[2] + 1))
    #
    # for si, swi, twi in output[30:60]:
    #     print "Sentence #%d: %s -> %s " % (si, _data.source_vocab[_data.sentence_pairs_idxs[si][0][swi]],
    #                                        _data.target_vocab[_data.sentence_pairs_idxs[si][1][twi]], )

    # print output[-10:]# print output[:10]
