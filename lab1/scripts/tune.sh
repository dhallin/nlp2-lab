#!/usr/bin/env bash
for file in output/tune/*
do 
	a=$(basename $file) 
	b=${a%.*}
	scripts/wa_eval_align.pl data/test.wa.nonullalign $file >> results/tune/$b.txt
done
