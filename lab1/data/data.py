__author__ = 'Daniel'

from itertools import izip

default_source_path = "data/corpus_1000.en"
default_target_path = "data/corpus_1000.nl"

null_word = "<#-NW-#>"


class Data:

    def __init__(self,
                 source_path=default_source_path,
                 target_path=default_target_path,
                 test_source_path=None,
                 test_target_path=None,
                 add_null_word=False, limit=None):

        self.source_vocab = []
        self.target_vocab = []
        self.sentence_pairs = []
        self.sentence_limit = limit

        self.source_word2idx = dict()
        self.target_word2idx = dict()
        self.sentence_pairs_idxs = []

        self.source_vocab_max_length = 0
        self.target_vocab_max_length = 0

        self.setup(source_path, target_path, test_source_path, test_target_path,  add_null_word)

    def setup(self, source_path, target_path, test_source_path, test_target_path, add_null_word):

        source_set = set()
        target_set = set()
        self.sentence_pairs = []
        self.test_sentence_pairs = []
        nw_idx = -1

        with open(source_path, 'r') as s_file, open(target_path, 'r') as t_file:
            for i,v in enumerate(izip(s_file, t_file)):
                if self.sentence_limit is not None and i >= self.sentence_limit:
                    break
                ss, ts = v
                ss = ss.rstrip()
                ts = ts.rstrip()
                self.sentence_pairs.append((ss, ts))
                source_set.update(ss.split(" "))
                target_set.update(ts.split(" "))

        if test_source_path is not None:
            with open(test_source_path, 'r') as s_file, open(test_target_path, 'r') as t_file:
                for ss, ts in izip(s_file, t_file):
                    ss = ss.rstrip()
                    ts = ts.rstrip()
                    self.sentence_pairs.append((ss, ts))
                    source_set.update(ss.split(" "))
                    target_set.update(ts.split(" "))

                    self.test_sentence_pairs.append((ss, ts))

        self.source_vocab = list(source_set)
        if add_null_word:
            nw_idx = len(self.source_vocab)
            self.source_vocab.append(null_word)
        self.source_word2idx = dict()

        for i, word in enumerate(self.source_vocab):
            self.source_word2idx[word] = i

        self.target_vocab = list(target_set)
        self.target_word2idx = dict()

        for i, word in enumerate(self.target_vocab):
            self.target_word2idx[word] = i

        self.sentence_pairs_idxs = []
        self.test_sentence_pairs_idxs = []

        for (ss, ts) in self.sentence_pairs:
            s_idxs = [self.source_word2idx[w] for w in ss.split(" ")]
            if add_null_word:
                s_idxs.append(nw_idx)
            t_idxs = [self.target_word2idx[w] for w in ts.split(" ")]
            self.sentence_pairs_idxs.append((s_idxs, t_idxs))
            self.source_vocab_max_length = max(self.source_vocab_max_length, len(s_idxs))
            self.target_vocab_max_length = max(self.target_vocab_max_length, len(t_idxs))

        if test_source_path is not None:
            for (ss, ts) in self.test_sentence_pairs:
                s_idxs = [self.source_word2idx[w] for w in ss.split(" ")]
                if add_null_word:
                    s_idxs.append(nw_idx)
                t_idxs = [self.target_word2idx[w] for w in ts.split(" ")]
                self.test_sentence_pairs_idxs.append((s_idxs, t_idxs))
                self.source_vocab_max_length = max(self.source_vocab_max_length, len(s_idxs))
                self.target_vocab_max_length = max(self.target_vocab_max_length, len(t_idxs))
        else:
            self.test_sentence_pairs_idxs = self.sentence_pairs_idxs
            self.test_sentence_pairs = self.sentence_pairs
        print "Data loaded: train-set: %d pairs, test-set: %d pairs" % (len(self.sentence_pairs), len(self.test_sentence_pairs))
        print "Source vocabulary size: %d" % len(self.source_vocab)
        print "Target vocabulary size: %d" % len(self.target_vocab)

if __name__ == "__main__":
    print "Initialising default data"
    d = Data()
    print "Initialisation complete"

    # Tests
    print "Start running tests"

    for i, word in enumerate(d.source_vocab):   # Test dictionaries
        assert i == d.source_word2idx[word]
        assert i+1 != d.source_word2idx[word]
    for i, word in enumerate(d.target_vocab):
        assert i == d.target_word2idx[word]
        assert i+1 != d.target_word2idx[word]

    for word in d.source_vocab:
        assert word != '\n'
    for word in d.target_vocab:
        assert word != '\n'

    print "Finished running tests\n" \
          "Done"



