\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
 \usepackage{multirow}
 \usepackage{rotating}
 \usepackage[numbers]{natbib}
\title{NLP2 - Lab 1}

\author{Bas Veeling (10767770) \\ Daniel Hallin (10538038)}

\begin{document}
\maketitle

\begin{abstract}
In this report, implementations of the translation models IBM1, IBM2 and modified versions are explained. The models are trained on the Hansard corpus and evaluated on a test set using a gold standard. The impact of the modifications are evaluated and the causes are reasoned about.
\end{abstract}

\section{Introduction}
% Introduce the problem: IBM Models, their limits, word allignment
To this day, IBM Model 1 and 2 are at the core of Machine Translation (MT) models. The initial IBM 1 model is an essential tool for lexical translations, and IBM2 builds upon this to set a baseline for word alignments. The models often form a fundamental part of modern MT methods, and their output can be used for various parameter initializations. It is thus worthwhile to evaluate the performance of these models, and explore potential improvements.


For this paper, both models have been implemented. We introduce various modifications and tweaks as suggested by \citet{moore2004improving}. We evaluate these additions on the Hansard corpus and compare them against their barebones counterparts. The impact of the modifications are deduced from the results and are explained and justified in the discussion.

\section{Methodology}
Implementations of both models are based on the lecture notes by Michael Collins. The models are implemented in Python, using various libraries from \texttt{Numpy} and \texttt{SciPy} where applicable.

\subsection{IBM Model 1}
In essence, IBM Model 1 learns correspondences of source and target words from sentence pairs. From these correspondences, it can infer lexical translations of individual words, which in turn are used to produce relatively crude alignments. 
In \citet{moore2004improving}, three improvements for these models are introduced:

\begin{enumerate}
\item Smoothing counts for rare words
\item Adding more probability mass for Null words
\item Use heuristics to initialize lexical parameters
\end{enumerate}
\paragraph{1. Smoothing Counts}

Certain words in dataset occur very infrequently. The model thus has a very limited amount of information about these words, and inference about them is problematic.  \citet{moore2004improving} identifies that rare words from the source set act as \emph{garbage-collectors} by aligning to too many words in the target language. In order to work with sentences in which these rare words incur,  \citeauthor{moore2004improving} identifies that smoothing the translation probability estimates leads to better performance. Specifically, we add virtual counts to each target word according to a uniform distribution:
\begin{align*}
tr(t|s) = \frac{C(t,s) + n}{C(s) + n|V|}
\end{align*}
Where $|V|$ is the target vocabulary size, $C(t,s)$ is the expected count of $s$ generating $t$, and $n$ is the added count. 

\paragraph{2. Adding Null Word Probability Mass}
\label{par:nullword}
IBM1 often suffers from a lack of alignments to the null source word. The modification is achieved by multiplying the null word probabilities by a positive scalar. 

\paragraph{3. Heuristic for initializing lexical parameters}
As a final improvement, Moore argues that despite the guarantee that EM will naturally converge to the global optimum due the the convexity of the IBM model, this might not always be the ideal optimum. It is proposed that initializing the lexical parameters in a non-uniform way can lead to closer convergence to the ideal optimum, and makes the model converge within a lot fewer iterations. 
We verify this by implementing the proposed heuristic: the log-likelihood ratio (LLR) as suggested by \citet{dunning1993accurate}. We compute the normalized LLR scores and set these as the initial translation probabilities and the initial null word probability is set to the target-word distribution, times the additional null word weight from paragraph~\ref{par:nullword}.

% introduce dataset
% experiment parameters

\subsection{IBM model 2} % (fold)
The next version of the IBM translation models, model 2, builds upon the initial model by introducing alignment probabilities inferred from word positions. As model 2 is non-convex, convergence of the EM algorithm can prove troublesome. We experiment with three initializations of the parameters to find a approach that speeds up convergence to a good optimum. These are:
\begin{enumerate}
\item Uniform initializations
\item Random initializations
\item IBM1 initializations
\end{enumerate}

The first two are trivial methods, the latter uses the lexical parameters learned from the IBM1 model as an starting point for model 2. We perform three runs of the random initialization, in order to evaluate the effect of starting out from different points.

\section{Experiments}
\subsection{Experimental Setup}
In order to evaluate the proposed methods, we train the models on the Hansard corpus. Our training dataset consists of the training corpus and the smaller test corpus. We only use the first 50.000 sentence-pairs from the training corpus due to time/computational constraints and technical difficulties with our implementation. Ideally, the full training set of ~200.000 pairs would be used, but this did not fit the time and memory budget. We run each model for 20 iterations.
The models then generate Viterbi-alignments for the test set, which are consequentially evaluated on a gold standard with the evaluation script by Rada Mihalcea. We report the AER score for our methods.

\subsection{IBM 1 modification parameter grid-search}
In order to find the right combination of parameters for the model 1 improvements, we performed a grid search run on the first 10000 sentence-pairs of the Hansard dataset. We investigate an even range of variables. For the smoothing the vocabulary length ($|V|$) and extra counts ($n$) are set to a factor and a percentage of the observed vocabulary length. The variables investigated and the results are shown appendix~\ref{app:gridsearch}. 
We conclude that in our implementation a uniform initialization performs best, without smoothing and with a null-word count of $10$. It is surprising that only one of the suggested changes seam to improve the performance of IBM Model 1, still not observed in appendix~\ref{app:gridsearch}, the Heuristic-initialization did converge within only 4-5 iterations whereas uniform distribution did in the range of 10 iterations. This suggests that given a lower number of iterations than what we used in our experiments (20), this improvement may show some comparably better results. 

\subsection{IBM 1 modified versus barebones}
In table \ref{tbl:ibm1-mod} the results for both models are shown, first trained on the 500000 first sentence pairs, secondly on the whole dataset. As anticipated, the improved model, \textbf{IBM 1 - Moore}, scores better on all measures. Further we see that the models trained on the complete dataset indeed, without surprise, perform better than both of its precedents. In figure~\ref{fig:ibm1moore-init} we can observe the convergence of the two models (for the 50000 first sentence pairs), and they perform similarly in speed and optima. As stated earlier, IBM1 - Moore models with heuristic initialization tended to converge significantly much faster.

\begin{table}[h1]

\begin{tabular}{l||lll|lll|l}
\multirow{2}{*}{\textbf{Method}} & \multicolumn{3}{l}{\textbf{Sure}}       & \multicolumn{3}{l}{\textbf{Probable}} & \multirow{2}{*}{\textbf{AER}}   \\ 
                        & Precision & Recall & F-measure & Precision & Recall & F-measure &  \\\hline
IBM Model 1 ($a$)                 &  0.4088  &  0.7858&  0.5378&  0.5793&  0.2578&  0.3568&  0.3500 \\
IBM 1 - Moore ($a$)       & \textbf{0.4124} & \textbf{0.7927} & \textbf{0.5426} & \textbf{0.5797} &\textbf{ 0.2580} & \textbf{0.3571} & \textbf{0.3474} \\ \hline
IBM Model 1 ($b$)        &  0.4048  & 0.7781& 0.5326& 0.5913& 0.2632& 0.3642& 0.3448 \\
IBM 1 - Moore ($b$)      & \textbf{0.4127}   &\textbf{0.7932}    & \textbf{0.5429}   & \textbf{0.5944}   & \textbf{0.2645}   & \textbf{0.3661}   & \textbf{0.3376}  \\

\end{tabular}
  \caption{ \label{tbl:ibm1-mod} Performance IBM 1 and IBM1-Moore. $a$ tests run on first 50000 sentence pairs. $b$ tests run on all sentence pairs.}
\end{table}

\begin{figure}[h!]
  \centering
        \includegraphics[width=0.8\textwidth]{../output/IBM1andMoore}
  \caption{\label{fig:ibm1moore-init} Convergence of IBM 1 and IBM1-Moore}

  
\end{figure}

\subsection{IBM 2 model}
As described in the previous section, three initializations of the IBM 2 model have been implemented. The log-likelihood convergence of these three methods are shown in figure~\ref{fig:ibm2-init}. The results show that the lexical initialization leads to a boost in the initial iterations, after which the random and uniform initialization quickly catch up. All three methods roughly converge towards the same likelihood,  with uniform having the highest final value.

In table~\ref{tbl:ibm2-init} we present the performance metrics of these three methods as computed with the gold standard, where we use the best performing random initialization. We see that on all measures, uniform gains the best score. Random performs similarly, but the lexical initialization, even though it is converging a lot faster than its competitors, is performing significantly worse. The faster convergence can easily be attributed to the already trained translation model, and the non-optimal optima is likely explained by an over-fitted initialization. This meaning that the IBM1 translation model has an optimum non-equal to the one of IBM2, which likelihood function is non-convex, and by training for to many iterations it does not diverge from this local optimum. 
This could be avoided by performing early stopping and performing fewer iterations in the IBM1 phase. 

\begin{figure}[h!]
  \centering
        \includegraphics[width=0.8\textwidth]{../output/lexical_random_uniform_loglike}
  \caption{\label{fig:ibm2-init} Convergence of IBM 2 with uniform, random (3 runs) and lexical (IBM1) initialization}

  
\end{figure}

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[h1]

\begin{tabular}{l|lll|lll|l}
\multirow{2}{*}{\textbf{Method}} & \multicolumn{3}{l}{\textbf{Sure}}       & \multicolumn{3}{l}{\textbf{Probable}} & \multirow{2}{*}{\textbf{AER}}   \\ 
                        & Precision & Recall & F-measure & Precision & Recall & F-measure &  \\\hline
Uniform                 & \textbf{0.4145}  & \textbf{0.7967}  &  \textbf{0.5453 }  &   \textbf{0.6604}   & \textbf{0.2939}  & \textbf{0.4068}   & \textbf{0.2930}\\
Random                  & 0.4140           & 0.7957       &  0.5446         &  0.6560         &  0.2919      & 0.4041          & 0.2962\\
Lexical                 & 0.3525          & 0.6776      & 0.4638    &  0.5997    & 0.2669    &  0.36 & 0.3737          
\end{tabular}
  \caption{ \label{tbl:ibm2-init} Performance IBM 2 with uniform, random and lexical (bold is best)}
  
\end{table}

\section{Conclusion}
We have implemented and evaluated IBM model 1 and 2, and various modifications on the former. All models have been evaluated on the Hansard dataset with the complete set and a subset with 50.000 sentence pairs. We have empirically shown that IBM2 with its non-uniform alignment-model performs better than IBM1 and that we by implementing some of the suggested improvements from Brown \citet{brown1993mathematics} we are able to improve the performance of IBM1. 
For model 2, the best performing initializations method is Uniform, with Random following close behind and lexical initialization not performing as well. We attribute this to the stochastic nature of random initialization is unpredictable and highly dependent on its initial values. We do believe that given sufficient runs this might very well outperform uniform initialization in terms of speed and optimum. A lexical prior leads to faster convergence but may lead to non-global optima.  

\bibliographystyle{plainnat}
\bibliography{biblio}
\appendix
\section{Appendix: Gridsearch table}
\label{app:gridsearch}

\input{gridsearch}


\end{document}
